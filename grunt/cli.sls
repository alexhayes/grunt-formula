---
{% from 'grunt/settings.jinja' import settings with context %}

include:
  - nodejs

grunt.cli:
  npm.installed:
    - name: {{ settings.pkg_cli }}
    - require:
      - pkg: nodejs
