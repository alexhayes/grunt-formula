---
{% from 'grunt/settings.jinja' import settings with context %}

include:
  - nodejs

grunt:
  npm.installed:
    - name: {{ settings.pkg }}
    - require:
      - pkg: nodejs

{% if settings.logdir %}
grunt.log:
  file.managed:
    - name: '{{ settings.logdir }}/grunt.log'
    - source: ~
    - makedirs: True
    - require_in:
      - npm: grunt

grunt.log.error:
  file.managed:
    - name: '{{ settings.logdir }}/grunt.error.log'
    - source: ~
    - makedirs: True
    - require_in:
      - npm: grunt
{% endif %}
